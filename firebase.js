import { getApp, getApps, initializeApp } from "firebase/app";
import { getFirestore } from "firebase/firestore";
import { getStorage } from "firebase/storage";

const firebaseConfig = {
  apiKey: "AIzaSyC8Pxc1tbkJt9Ztzb3C7shgwvKzH0wt3zc",
  authDomain: "facebook-clone-de099.firebaseapp.com",
  projectId: "facebook-clone-de099",
  storageBucket: "facebook-clone-de099.appspot.com",
  messagingSenderId: "626373483068",
  appId: "1:626373483068:web:d6f0ea5d51b168a1d38139"
};

// Initialize Firebase
const app = !getApps().length ? initializeApp(firebaseConfig) : getApp();

const db = getFirestore()
const storage = getStorage()

export { db, storage };