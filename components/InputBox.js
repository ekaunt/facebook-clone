import { useSession } from "next-auth/client";
import Image from "next/image";
import { EmojiHappyIcon } from "@heroicons/react/outline"
import { CameraIcon, VideoCameraIcon } from "@heroicons/react/solid"
import { useRef, useState } from "react";
import { db, storage } from "../firebase";
import { addDoc, collection, doc, serverTimestamp, setDoc } from "firebase/firestore";
import { getDownloadURL, ref, uploadString } from "firebase/storage";


function InputBox() {
	const [session] = useSession()
	const inputRef = useRef(null)
	const filepickerRef = useRef(null)
	const [imageToPost, setImageToPost] = useState(null)

	function sendPost(e) {
		e.preventDefault();
		
		if (!inputRef.current.value) return;

		addDoc(collection(db, "posts"), {
			message: inputRef.current.value,
			name: session.user.name,
			email: session.user.email,
			image: session.user.image,
			timestamp: serverTimestamp()
		}).then(thisdoc => {
			if (imageToPost) {
				//upload image
				uploadString(ref(storage, `posts/${thisdoc.id}`), imageToPost, 'data_url').then((snapshot) => {
					getDownloadURL(snapshot.ref).then(url => {
						setDoc(doc(db, 'posts', thisdoc.id), {
							postImage: url
						}, {merge: true})
					})
				})
				removeImage()
			}
		})

		inputRef.current.value = "";
	}

	const addImageToPost = (e) => {
		const reader = new FileReader();
		if (e.target.files[0]) {
			reader.readAsDataURL(e.target.files[0])
		}

		reader.onload = (readerEvent) => {
			setImageToPost(readerEvent.target.result)
		}
	}

	const removeImage = () => {
		setImageToPost(null)
	}

	return (
		<div className='bg-white p-2 rounded-2xl shadow-sm text-gray-500 font-medium mt-6'>
			<div className='flex space-x-4 pb-2 items-center'>
				<Image 
					className='rounded-full'
					src={session.user.image}
					width={40}
					height={40}
					layout="fixed"
					alt=""
				/>
				<form className='flex flex-1'>
					<input type='text' ref={inputRef} placeholder={`What's on your mind ${session.user.name}?`} className='rounded-full h-12 bg-gray-100 flex-grow px-5 focus:outline-none' />
					<button hidden onClick={sendPost}></button>
				</form>

				{imageToPost && (
					<div onClick={removeImage} className='flex flex-col filter hover:brightness-110 transition duration-150 transform hover:scale-105 cursor-pointer'>
						<img className="h-10 object-contain" src={imageToPost} alt="" />
						<p className='text-xs text-red-500 text-center'>Remove</p>
					</div>
				)}
			</div>

			<div className='flex justify-evenly p-3 border-t'>
				<div className='input-icon'>
					<VideoCameraIcon className='h-7 text-red-500' />
					<p className='text-xs sm:text-sm lg:text-base'>Live Video</p>
				</div>

				<div onClick={() => filepickerRef.current.click()} className='input-icon'>
					<CameraIcon className='h-7 text-green-500' />
					<p className='text-xs sm:text-sm lg:text-base'>Photo/Video</p>
					<input ref={filepickerRef} onChange={addImageToPost} type='file' hidden />
				</div>

				<div className='input-icon'>
					<EmojiHappyIcon className='h-7 text-yellow-500' />
					<p className='text-xs sm:text-sm lg:text-base'>Feeling/Activity</p>
				</div>
			</div>
		</div>
	)
}

export default InputBox
