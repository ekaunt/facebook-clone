import { SearchIcon } from "@heroicons/react/outline"
import { DotsHorizontalIcon, VideoCameraIcon } from "@heroicons/react/solid"
import Contact from "./Contact"
import profile2 from "/images/profile2.jpg"
import profile3 from "/images/profile3.jpg"
import profile4 from "/images/profile4.jpg"
import profile5 from "/images/profile5.jpg"
import profile6 from "/images/profile6.webp"
import profile7 from "/images/profile7.webp"
import profile8 from "/images/profile8.jpg"

const contacts = [
	{ src: profile3, name: "Jeff Bezos" },
	{ src: profile2, name: "Elon Musk" },
	{ src: profile5, name: "Bill Gates" },
	{ src: profile4, name: "Mark Zuckerberg" },
	{ src: profile6, name: "Harry Potter" },
	{ src: profile7, name: "The Queen" },
	{ src: profile8, name: "James Bond" },
]

function Widgets() {
	return (
		<div className='hidden lg:flex flex-col w-60 p-2 mt-5'>
			<div className='flex justify-between items-center text-gray-500 mb-5'>
				<h2 className='text-xl'>Contacts</h2>
				<div className='flex space-x-2'>
					<VideoCameraIcon className='h-6' />
					<SearchIcon className='h-6' />
					<DotsHorizontalIcon className='h-6' />
				</div>
			</div>

			{contacts.map(contact => (
				<Contact key={contact.src} src={contact.src} name={contact.name} />
			))}
		</div>
	)
}

export default Widgets
