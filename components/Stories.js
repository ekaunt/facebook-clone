import StoryCard from "./StoryCard"
import bg1 from "/images/bg1.jpg"
import profile1 from "/images/profile1.jpg"
import bg2 from "/images/bg2.jpg"
import profile2 from "/images/profile2.jpg"
import bg3 from "/images/bg3.webp"
import profile3 from "/images/profile3.jpg"
import bg4 from "/images/bg4.jpg"
import profile4 from "/images/profile4.jpg"
import bg5 from "/images/bg5.jpg"
import profile5 from "/images/profile5.jpg"

const stories = [
	{
		name: "Steve Jobs",
		src: bg1,
		profile: profile1
	},
	{
		name: "Elon Musk",
		src: bg2,
		profile: profile2
	},
	{
		name: "Jeff Bezos",
		src: bg3,
		profile: profile3
	},
	{
		name: "Mark Zuckerberg",
		src: bg4,
		profile: profile4
	},
	{
		name: "Bill Gates",
		src: bg5,
		profile: profile5
	},
]

function Stories() {
	return (
		<div className="flex justify-center space-x-3 mx-auto">
			{stories.map(story => (
				<StoryCard 
					key={story.src} 
					name={story.name} 
					src={story.src} 
					profile={story.profile} 
				/>
			))}
		</div>
	)
}

export default Stories
